<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('all', 'TodoModelController@all')->name('all');
Route::post('complete', 'TodoModelController@complete')->name('complete');
Route::get('destroy/{id}', 'TodoModelController@destroy')->name('destroy');
Route::resource('/', 'TodoModelController');
