<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="To do Application." />
    <link rel="manifest" href="manifest.json">
    <meta name="theme-color" content="#317EFB" />
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="msapplication-starturl" content="/">
    <link rel="apple-touch-icon" href="/images/icons/icon-192x192.png">
    <title>Todo Application!</title>
    <script>
        // Check that service workers are supported
        if ('serviceWorker' in navigator) {
          // Use the window load event to keep the page load performant
          window.addEventListener('load', () => {
            navigator.serviceWorker.register('/webpushr-sw.js');
          });
        }
    </script>
    <link rel="stylesheet" href="{{ mix('/css/app.css') }}">
    {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.2/css/bulma.min.css"> --}}
    <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>


</head>

<body>
    <style>
        .saveTodo {
            display: none;
            position: absolute;
            right: 50px;
            bottom: 4px;
        }

        .show {
            display: block;
        }
    </style>
    <header>
        <nav class="navbar" role="navigation" aria-label="main navigation">
            <div class="navbar-brand">
                <a class="navbar-item" href="https://bulma.io">
                    <img src="https://bulma.io/images/bulma-logo.png" alt="ToDO" width="112" height="28">
                </a>

                <a role="button" class="navbar-burger burger" aria-label="menu" aria-expanded="false"
                    data-target="navbarBasicExample">
                    <span aria-hidden="true"></span>
                    <span aria-hidden="true"></span>
                    <span aria-hidden="true"></span>
                </a>
            </div>

            <div id="navbarBasicExample" class="navbar-menu">
                <div class="navbar-start">
                    <a class="navbar-item">
                        Home
                    </a>

                    <a class="navbar-item">
                        Documentation
                    </a>

                    <div class="navbar-item has-dropdown is-hoverable">
                        <a class="navbar-link">
                            More
                        </a>

                        <div class="navbar-dropdown">
                            <a class="navbar-item">
                                About
                            </a>
                            <a class="navbar-item">
                                Jobs
                            </a>
                            <a class="navbar-item">
                                Contact
                            </a>
                            <hr class="navbar-divider">
                            <a class="navbar-item">
                                Report an issue
                            </a>
                        </div>
                    </div>
                </div>

                <div class="navbar-end">
                    <div class="navbar-item">
                        <div class="buttons">
                            <a class="button is-primary">
                                <strong>Sign up</strong>
                            </a>
                            <a class="button is-light">
                                Log in
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    </header>
    <section class="section">
        <div class="container">
            <ul>
                <li>
                    <textarea id="todoText" class="textarea" placeholder="e.g. Wash Utesils"></textarea>
                </li>

                <br id="toadd">

            </ul>
        </div>
    </section>
    <div>
        <a href="#" class="saveTodo">
            <span class="icon is-large has-text-success">
                <i class="fas fa-3x fa-plus-circle"></i>
            </span>
        </a>
    </div>

    <script sync src="{{ mix('/js/app.js') }}"></script>

    <script defer>
        var url = '{{ route('all') }}';
        var urlsave = '{{ route('store') }}';
        var urlcomplete = '{{ route('complete') }}';
        var pri = '{{ url('/') }}';
        document.addEventListener('DOMContentLoaded', function () {
            try {
                var el = document.querySelector('.loding');
                console.log(el);
                el.className = null;
            } catch (error) {

            }

            var todoText = document.querySelector('#todoText');
            todoText.addEventListener('focus', function (event) {
                //display buttons
                const savebutton = document.querySelector('.saveTodo');
                savebutton.className += " show";
                savebutton.onclick = function (params) {
                    console.log('buttonclicked');
///save text box content
const options = {
        url: urlsave,
    method: 'POST',
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json;charset=UTF-8'
    },
    data:{
        details:event.target.value
    }
};

axios(options)
    .then(successSave)
    .catch($error=>{
        alert('We temporary failed to save this to do. Try again in a sec!');
        console.log($error);
    });

                    event.target.value=null;
                }
                console.log('display buttions');
                //get track of added text
               /*  event.target.addEventListener('input', (e) => {
                    console.log(e.target.value);
                }); */
            });


            const $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

            // Check if there are any navbar burgers
            if ($navbarBurgers.length > 0) {

                // Add a click event on each of them
                $navbarBurgers.forEach(el => {
                    el.addEventListener('click', () => {

                        // Get the target from the "data-target" attribute
                        const target = el.dataset.target;
                        const $target = document.getElementById(target);

                        // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
                        el.classList.toggle('is-active');
                        $target.classList.toggle('is-active');

                    });
                });
            }
        })

        function successSave(response) {
            console.log(response);
if (response.data.status=='success') {
    const savebutton = document.querySelector('.saveTodo');
                savebutton.classList.toggle("show");

                toaddd = document.getElementById('toadd');
                $sta = response.data.item.status == 1 ? 'is-primary' : '';

                toaddd.insertAdjacentHTML('afterend', `<li id ='${response.data.item.id}'>
                    <button class="button is-success" id='complete'>Complete</button>

            <button class="button is-danger" id='delete'>Delete</button>
         <div class="notification ${$sta}">

             ${ response.data.item.details}
         </div>
     </li> <br>`);

}else{
    alert('Enter some info before you save');
}
        }
    </script>

    <script defer src="{{ mix('/js/todo.js') }}"></script>
    <!-- start webpushr tracking code -->
    <script defer>
        (function(w,d, s, id) {if(typeof(w.webpushr)!=='undefined') return;w.webpushr=w.webpushr||function(){(w.webpushr.q=w.webpushr.q||[]).push(arguments)};var js, fjs = d.getElementsByTagName(s)[0];js = d.createElement(s); js.id = id;js.src = "https://cdn.webpushr.com/app.min.js";
    fjs.parentNode.appendChild(js);}(window,document, 'script', 'webpushr-jssdk'));
    webpushr('init','BNDWyJ0Ycc4cvqLlJ144NhsMjNNn2fOAUbitzVzSaVJDCtzotVeUZ-WRMDwf6xbMm3rst-zjY6w28S3GOAlMz6A');
    </script>
    <!-- end webpushr tracking code -->
    {{-- <script rel="preconnect" id="__bs_script__">
        //<![CDATA[
    document.write("<script async src='http://HOST:3000/browser-sync/browser-sync-client.js?v=2.26.7'><\/script>".replace("HOST", location.hostname));
//]]>
    </script> --}}

</body>

</html>
