<?php

use Illuminate\Database\Seeder;

class TodoModelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\TodoModel::class, 10)->create();
    }
}
