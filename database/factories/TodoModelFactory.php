<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\TodoModel;
use Faker\Generator as Faker;

$factory->define(TodoModel::class, function (Faker $faker) {
    return [
        'details'=>$faker->paragraph,
        'status'=>$faker->randomElement([0,1])
    ];
});
