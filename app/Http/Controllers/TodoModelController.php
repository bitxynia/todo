<?php

namespace App\Http\Controllers;

use App\TodoModel;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class TodoModelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('todo.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //return response()->json(['status'=>$request->details]);
        $val = Validator::make(['details' => $request->details], [
            'details' => 'required',
        ]);

        if ($val->fails()) {
            return ($val->errors());
        }

        try {
            DB::beginTransaction();
            $item =   TodoModel::create([
                'details' => $request->details
            ]);
            DB::commit();
        } catch (\Throwable $th) {
            return response()->json(['error' => 'check your data']);
        }
        return response()->json(['status' => 'success', 'item' => $item]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TodoModel  $todoModel
     * @return \Illuminate\Http\Response
     */
    public function show(TodoModel $todoModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TodoModel  $todoModel
     * @return \Illuminate\Http\Response
     */
    public function edit(TodoModel $todoModel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TodoModel  $todoModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TodoModel $todoModel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TodoModel  $todoModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(TodoModel $id)
    {

        try {
            $id->delete();
        } catch (\Throwable $th) {
            return response()->json([
                'error' =>'Failed to update the record'
            ]);
        }
        return response()->json([
            'status' => 'success'
        ]);

    }

    public function all()
    {
        $data = TodoModel::all();
        return  response()->json($data);
    }

    public function complete(Request $request)
    {
        $r = ($request->data);

        try {
            $finsh = TodoModel::find($r['id']);
            $finsh->status = 1;
            $finsh->save();
        } catch (\Throwable $th) {

            return response()->json([
                'error' =>'Failed to update the record'
            ]);
        }
        return response()->json([
            'status' => 'success'
        ]);
    }
}
